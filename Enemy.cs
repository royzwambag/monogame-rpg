﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RPG
{
    internal class Enemy
    {
        public static readonly List<Enemy> enemies = new List<Enemy>();

        private Vector2 position;
        private const int speed = 150;
        private const int radius = 30;
        public readonly SpriteAnimation Animation;

        public Enemy(Vector2 newPosition, Texture2D spriteSheet)
        {
            position = newPosition;
            Animation = new SpriteAnimation(spriteSheet, 10, 6);
        }
        
        public Vector2 Position => position;
        public int Radius => radius;
        public bool Dead { get; set; }

        public void Update(GameTime gameTime, Vector2 playerPosition, bool playerDead)
        {
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            Animation.Position = new Vector2(position.X - 48, position.Y - 66);
            Animation.Update(gameTime);

            if(!playerDead)
            {
                Vector2 moveDirection = playerPosition - position;
                moveDirection.Normalize(); // Keep direction the same, but move length to 1
                position += moveDirection * speed * deltaTime;
            }
        }
    }
}