﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace RPG
{
    public class Controller
    {
        public static double timer = 2D;
        public static double maxTime = 2D;
        private static Random random = new Random();

        public static void Update(GameTime gameTime, Texture2D enemySpriteSheet)
        {
            timer -= gameTime.ElapsedGameTime.TotalSeconds;
            
            if (timer <= 0)
            {
                spawnEnemy(enemySpriteSheet);
                timer = maxTime;
                if (maxTime > 0.5)
                    maxTime -= 0.05D;
            }
        }

        private static void spawnEnemy(Texture2D enemySpriteSheet)
        {
            int spawnSide = random.Next(4);

            switch (spawnSide)
            {
                case 0:
                    Enemy.enemies.Add(new Enemy(new Vector2(-1000, random.Next(-1000, 3000)), enemySpriteSheet));
                    break;
                case 1:
                    Enemy.enemies.Add(new Enemy(new Vector2(3000, random.Next(-1000, 3000)), enemySpriteSheet));
                    break;
                case 2:
                    Enemy.enemies.Add(new Enemy(new Vector2(random.Next(-1000, 3000), -1000), enemySpriteSheet));
                    break;
                case 3:
                    Enemy.enemies.Add(new Enemy(new Vector2(random.Next(-1000, 3000), 3000), enemySpriteSheet));
                    break;
            }
        }
    }
}