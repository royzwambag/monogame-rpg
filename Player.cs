﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace RPG
{
    internal class Player
    {
        private Vector2 position = new Vector2(500, 300);
        private readonly int speed = 300;
        private Direction direction = Direction.Down;
        private bool isMoving = false;
        private KeyboardState previousKeyboardState = Keyboard.GetState();
        
        public SpriteAnimation animation;

        public SpriteAnimation[] animations = new SpriteAnimation[4];
        
        public bool Dead { get; set; }
        public Vector2 Position => position;

        public void SetX(float x)
        {
            position.X = x;
        }
        public void SetY(float y)
        {
            position.Y = y;
        }

        public void Update(GameTime gameTime)
        {
            KeyboardState keyboardState = Keyboard.GetState();
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (Dead)
                return;
            
            SetDirection(keyboardState);
            
            if (isMoving)
                Move(deltaTime);

            if (Shooting(keyboardState))
            {
                Shoot();
            }
            
            animation = animations[(int) direction];
            animation.Position = new Vector2(position.X - 48, position.Y - 48);

            if (Shooting(keyboardState))
                animation.SetFrame(0);
            else if (isMoving)
                animation.Update(gameTime);
            else
                animation.SetFrame(1);
            
            previousKeyboardState = keyboardState;
        }

        private void SetDirection(KeyboardState keyboardState)
        {
            if (keyboardState.IsKeyDown(Keys.Up))
            {
                direction = Direction.Up;
                    isMoving = true;
            } else if (keyboardState.IsKeyDown(Keys.Down))
            {
                direction = Direction.Down;
                isMoving = true;
            } else if (keyboardState.IsKeyDown(Keys.Left))
            {
                direction = Direction.Left;
                isMoving = true;
            } else if (keyboardState.IsKeyDown(Keys.Right))
            {
                direction = Direction.Right;
                isMoving = true;
            }
            else
            {
                isMoving = false;
            }
        }

        private void Move(float deltaTime)
        {
            switch (direction)
            {
                case Direction.Up:
                    if (position.Y > 200)
                        position.Y -= speed * deltaTime;
                    break;
                case Direction.Down:
                    if (position.Y < 1250)
                        position.Y += speed * deltaTime;
                    break;
                case Direction.Left:
                    if (position.X > 225)
                        position.X -= speed * deltaTime;
                    break;
                case Direction.Right:
                    if (position.X < 1275)
                        position.X += speed * deltaTime;
                    break;
            }
        }

        private void Shoot()
        {
            Projectile.projectiles.Add(new Projectile(position, direction));
            Sounds.projectileSound.Play(1f, 0.5f, 0f);
        }

        private bool Shooting(KeyboardState keyboardState)
        {
            return keyboardState.IsKeyDown(Keys.Space) && previousKeyboardState.IsKeyUp(Keys.Space);
        }
    }
}