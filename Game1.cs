﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Comora;

namespace RPG
{
    enum Direction
    {
        Up,
        Down,
        Left,
        Right,
    }

    public static class Sounds
    {
        public static SoundEffect projectileSound;
        public static Song backgroundMusic;
    }
    public class Game1 : Game
    {
        private readonly GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private Texture2D playerSprite;
        private Texture2D walkUp;
        private Texture2D walkDown;
        private Texture2D walkLeft;
        private Texture2D walkRight;

        private Texture2D background;
        private Texture2D ball;
        private Texture2D skull;

        private readonly Player player = new Player();

        private Camera camera;

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            _graphics.PreferredBackBufferWidth = 1280;
            _graphics.PreferredBackBufferHeight = 720;
            _graphics.ApplyChanges();

            camera = new Camera(_graphics.GraphicsDevice);
            
            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            playerSprite = Content.Load<Texture2D>("Player/player");
            walkUp = Content.Load<Texture2D>("Player/walkUp");
            walkDown = Content.Load<Texture2D>("Player/walkDown");
            walkLeft = Content.Load<Texture2D>("Player/walkLeft");
            walkRight = Content.Load<Texture2D>("Player/walkRight");

            background = Content.Load<Texture2D>("background");
            ball = Content.Load<Texture2D>("ball");
            skull = Content.Load<Texture2D>("skull");

            player.animations[(int)Direction.Up] = new SpriteAnimation(walkUp, 4, 6);
            player.animations[(int)Direction.Down] = new SpriteAnimation(walkDown, 4, 6);
            player.animations[(int)Direction.Left] = new SpriteAnimation(walkLeft, 4, 6);
            player.animations[(int)Direction.Right] = new SpriteAnimation(walkRight, 4, 6);
            player.animation = player.animations[(int) Direction.Down];

            Sounds.projectileSound = Content.Load<SoundEffect>("Sounds/blip");
            Sounds.backgroundMusic = Content.Load<Song>("Sounds/nature");
            MediaPlayer.Play(Sounds.backgroundMusic);
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            player.Update(gameTime);
            camera.Position = player.Position;
            camera.Update(gameTime);

            foreach (Enemy enemy in Enemy.enemies)
            {
                int sum = 32 + enemy.Radius;
                if (Vector2.Distance(player.Position, enemy.Position) < sum)
                    player.Dead = true;
                enemy.Update(gameTime, player.Position, player.Dead);
            }

            foreach (Projectile projectile in Projectile.projectiles)
                projectile.Update(gameTime);

            foreach (Projectile projectile in Projectile.projectiles)
            {
                foreach (Enemy enemy in Enemy.enemies)
                {
                    int sum = projectile.Radius + enemy.Radius;
                    if (Vector2.Distance(projectile.Position, enemy.Position) < sum)
                    {
                        projectile.Collided = true;
                        enemy.Dead = true;
                    }
                }    
            }

            Projectile.projectiles.RemoveAll(p => p.Collided);
            Enemy.enemies.RemoveAll(e => e.Dead);
            
            if (!player.Dead)
                Controller.Update(gameTime, skull);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin(camera);
            _spriteBatch.Draw(background, new Vector2(-500, -500), Color.White);
            
            foreach (Enemy enemy in Enemy.enemies)
                enemy.Animation.Draw(_spriteBatch);
            foreach (Projectile projectile in Projectile.projectiles)
                _spriteBatch.Draw(ball, projectile.Position, Color.White);

            if (!player.Dead)
                player.animation.Draw(_spriteBatch);

            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}