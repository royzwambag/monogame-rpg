﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace RPG
{
    internal class Projectile
    {
        public static readonly List<Projectile> projectiles = new List<Projectile>();

        private Vector2 position;
        private const int speed = 1000;
        private const int radius = 18;
        private readonly Direction direction;

        public Projectile(Vector2 newPosition, Direction newDirection)
        {
            position = newPosition;
            direction = newDirection;
        }

        public Vector2 Position => new Vector2(position.X - 48, position.Y - 48);
        public int Radius => radius;
        public bool Collided { get; set; }

        public void Update(GameTime gameTime)
        {
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            Move(deltaTime);
        }
        
        private void Move(float deltaTime)
        {
            switch (direction)
            {
                case Direction.Up:
                    position.Y -= speed * deltaTime;
                    break;
                case Direction.Down:
                    position.Y += speed * deltaTime;
                    break;
                case Direction.Left:
                    position.X -= speed * deltaTime;
                    break;
                case Direction.Right:
                    position.X += speed * deltaTime;
                    break;
            }
        }
    }
}